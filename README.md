# Sessions

Documentation and instructions for each hacking session.

Uses [mdbook](https://github.com/rust-lang/mdBook) to generate a gitbook.

## Development

To run a live preview of the site, download and install mdbook, then:

    $ mdbook serve --port 8000

Alternatively, use the provided docker-compose setup (exactly the same
versions and setup as production):

    $ docker-compose up

Whichever method you choose, you should have a live view of the site running
on <http://localhost:8000>.
