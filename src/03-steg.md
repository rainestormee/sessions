# Steganography session

In this session we will be looking at steganography, which is 
> The art or practice of concealing a message, image, or file within another
> message, image, or file.

We have prepared a series of files, all which have hidden messages or other
files in them, which you can find [here](https://shorturl.at/nEMQV)!

We have also prepared a short presentation, which contains info on a range
of tools you might find useful, and ZeroBadgers will give a walk-through 
in the session of the [HTB](https://hackthebox.eu) **BitsNBytes** challenge!

Happy hacking!

<p>
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQPUhLNq-vYa3R8_tlOtGIF5h1IOTow17xjNRLZJtrleFnGafCTPAoVHKgRfPAgN-b1pbbAO0EWib7j/embed?start=false&loop=false&delayms=3000" frameborder="0" width="720" height="449" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</p>
