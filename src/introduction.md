# Introduction

This book is a collection of instructions and material for each AFNOM
session over the 2020-21 period.

To see the source, and contribute patches, see <https://gitlab.com/afnom/sessions>.
