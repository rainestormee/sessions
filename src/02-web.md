# Web session

In this session we will be working our way through the [Over the Wire
Natas](https://overthewire.org/wargames/natas/) wargames. They teach
about how website work and what could go wrong!

Read the instructions on the Natas homepage, it will tell you how to 
get started. The wargame has 34 levels, see how many you can complete!

If you've done some of them, or find the first levels too easy,
we're putting below some of the level passwords, so you can skip ahead.
It's also great if you want to come back later and pick up from where 
you left, but forgot to take notes of the passwords (as neko3 often
forgets &#128517;)

The passwords are the ones to *enter* the levels mentioned (they're actually
there, just hidden for spoilers!):

- Lvl 5:  <span style="color:var(--bg)">iX6IOfmpN7AYOQGPwtn3fXpbaJVJcHfq</span>
- Lvl 10: <span style="color:var(--bg)">nOpp1igQAkUzaI1GUUjzn1bFVj7xCNzu</span>
- Lvl 15: <span style="color:var(--bg)">AwWj0w5cvxrZiONgZ9J5stNVkmxdk39J</span>
- Lvl 20: <span style="color:var(--bg)">eofm3Wsshxc5bwtVnEuGIlr7ivb9KABF</span>
- Lvl 25: <span style="color:var(--bg)">GHF6X7YwACaYYssHVY05cFq83hRktl4c</span>
- Lvl 30: <span style="color:var(--bg)">wie9iexae0Daihohv8vuu3cei9wahf0e</span>

If you've already done Natas, give us a shout in the session, we've got other
things for you to try!

Happy Web hacking!
