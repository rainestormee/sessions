# Linux session

In this session we will be working our way through the [Over the Wire
Bandit](https://overthewire.org/wargames/bandit/) wargames. They teach
about Linux command line, permissions, data encodings and more!

Read the instructions on the Bandit homepage, it will tell you how to 
get started. The wargame has 34 levels, see how many you can complete!
If this is your first incursion into ethical hacking, **start from the
beginning**.

If you've done some of them, or find the first levels too easy,
we're putting below some of the level passwords, so you can skip ahead.
It's also great if you want to come back later and pick up from where 
you left, but forgot to take notes of the passwords (as neko3 often
forgets &#128517;)

The passwords are the ones to *enter* the levels mentioned (they're actually
there, just hidden for spoilers!):

- Lvl 5:  <span style="color:var(--bg)">koReBOKuIDDepwhWk7jZC0RTdopnAYKh</span>
- Lvl 10: <span style="color:var(--bg)">truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk</span>
- Lvl 15: <span style="color:var(--bg)">BfMYroe26WYalil77FoDi9qh59eK5xNr</span>
- Lvl 20: <span style="color:var(--bg)">GbKksEFF4yrVs6il55v6gwY5aVje5f0j</span>
- Lvl 25: <span style="color:var(--bg)">uNG9O58gUE7snukf3bvZ0rxhtnjzSGzG</span>
- Lvl 30: <span style="color:var(--bg)">5b90576bedb2cc04c86a9e924ce42faf</span>

If you've already done Bandit, give us a shout in the session, we've got other
things for you to try!

Happy Linux hacking!
