# AFNOM NETWORKING

## INTRO:

Slides: [Networking Fun](https://docs.google.com/presentation/d/1Fc6hePFbFKTz9-enqfI9OCc2HxZomqsy1AvoEmNBgu4/edit?usp=sharing)

Announcements: [Blackhoodie](https://blackhoodie.re/Grehack/) and [HTB CTF](https://www.hackthebox.eu/universities/university-ctf-2020)

We're going to be doing some immersive labs as a warm up, then playing around with the chals below, and finally some CTF chals I amde for the CTF last year :)

**JUST ASK**

## Immersive labs

Would recommend you do the first 3 from:
[Immersive Labs Packet Analysis Tools](https://main.immersivelabs.online/cyber-skills/tools/skill-set/packet-analysis-tools)
Don't worry about the last 2

If you feel you need better background on networking you can also check out:

[Immersive Labs Networking](https://main.immersivelabs.online/cyber-skills/tools/skill-set/networking)

and

[Digital Ocean Intro to Netowrking](https://www.digitalocean.com/community/tutorials/an-introduction-to-networking-terminology-interfaces-and-protocols)

## Challenges

I've written a few short simple challenges to do on your own computer, where you'll carry out some tasks while recording them in wireshark and take a look at what you find. There'll be some overlap with the immersive chals but don't worry, skip over any bits you don't feel like doing, it's just for fun :)
If none of these interest you we've got some simplish pcap CTF chals at the end for you to have a crack at :)

Each challenge comes with some resources for if you don't know how to get started, with maybe some helpful linux commands to use, although 

### 0: Interfaces

On linux play around with the `ip` command (pro tip: use the magical -c option)

I want you to briefly take a look at the output, try to figure out what most of it means (if you don't know just ask, either someone else will know or you can figure it out together :)). I want you to work out what interface(s?) you're currently using for your m,ain connection to the internet.

Resources:
- `ip addr` `man ip`  
- [HowToGeek how to use ip command on Linux](https://www.howtogeek.com/657911/how-to-use-the-ip-command-on-linux/)

Task 1: Wireshark

Turn on wireshark and start live recording whichever interface you think you're using for your connection. Throughout these chals I want you to take a look at what the commands you write look like ont he wire and it's really interesting!

hint: Wireshark will show a graph of which ones are in use and which aren't, use this. Also if you think you got the right one but can't see amy traffic, you didn't pick the right one.

### 1: DNS-1

Manually find the IP address of `afnom.net` and of `wtctf.afnom.net`. Take a look at the DNS traffic in wireshark and see if you can follow the flow of what happened, how you asked for the information and how you were answered.

Resources:
- `man nslookup` `man dig`  
- [Linux nslookup command](https://www.computerhope.com/unix/unslooku.htm)

### 2: OS from PING

Now ping the IP address you retrieved for part 1. Look at this in wireshark, and see what packet types ping uses.

See if you can use a cool networking trick to work out what Operating System the afnom.net server is running.

Resources:

- `man ping`  
- [How To Identify The Operating System Using PING Command](https://www.yeahhub.com/identify-operating-system-using-ping-command/)
- [Internet protocol RFC 791](https://tools.ietf.org/html/rfc791)

### 3: traceroute

`traceroute` is a command that uses ping to trace which route a packet will take to a destination. Use it to find the route to afnom.net and try to understand that.

Resources:
- check this out too: [traceroute](https://gsuite.tools/traceroute)

### 4: HTTP vs HTTPS

In your browser visit [http://example.com](http://example.com/) and [https://example.com/](https://example.com/) (one is http the other is http**s**).

Look in wireshark and see how you can see the contents of plain HTTP but not the encrypted traffic that is HTTPS.

You can investigate the TLS handshake mechanism if you're interested by looking at the packets and comparing to: [What Happens in a TLS Handshake?](https://www.cloudflare.com/en-gb/learning/ssl/what-happens-in-a-tls-handshake/)

### 5: DNS

By now you should have been generating *loads* of DNS traffic. filter for DNS, look back through your capture so far and see how much information it has / what weird services you can see your device connecting to.

### 6: SSH

See what SSH traffic looks like between you and tinky-winky

### 7: netstat

Play around with the `netstat` command on linux to explore what services your device is running right now (might explain some of the weird traffic you've been seeing)

2 good commands to try are:

 - `sudo netstat -plnt` list listening TCP services
 - `sudo netstat -plant` list active TCP services

### 8: Tshark

There's a python library for reading and doing cool stuff with pcaps, *super* useful for CTFs

[PyShark](https://thepacketgeek.com/pyshark/)

Try using it to solve the `suspicious` chal below :)

## CTF challenges

I've got 3 PCAP based chals from our first CTF, sort of ordered by easy to hard. All flags are in the format AFNOM{flag}. To be honest they're all very small, self contained and clean, as opposed to the gigantic sprawling pcaps you tend to see in real life so while they're quite fun they're neither the hardest nor the most realistic.

Drox (while very amusing) isn't necesarily a great networking challenge so feel free to skip it

If you want more ask: we have some from last years as well + other random CTF's we've done

# THE END

Happy hacking :)
